## Ссылки приложения!

* cats-api-like - http://meowle.qa-fintech.tcsbank.ru/api/likes (Swagger http://meowle.qa-fintech.tcsbank.ru/api/likes/api-docs-ui/)
* cats-api-photo - http://meowle.qa-fintech.tcsbank.ru/api/photos (Swagger http://meowle.qa-fintech.tcsbank.ru/api/photos/api-docs-ui/)
* cats-api-core - http://meowle.qa-fintech.tcsbank.ru/api/core (Swagger http://meowle.qa-fintech.tcsbank.ru/api/core/api-docs-ui/)

Версия приложения:

* cats-api-like - http://meowle.qa-fintech.tcsbank.ru/api/likes/status
* cats-api-photo - http://meowle.qa-fintech.tcsbank.ru/api/photos/status
* cats-api-core - http://meowle.qa-fintech.tcsbank.ru/api/core/status

## Тестовый стенд

После создания merge request в гитлабе - можно запустить джобу для разворачивания изменений на тестовом стенде. Что бы посмотреть эти изменения необходимо зайти на http://meowle.qa-fintech.tcsbank.ru и добавить куку `stage_api=1` (инструменты разработчика -> вкладка Application -> cookies -> meowle -> в пустой колонке Name вводите `stage_api` -> в пустой колонке Value вводите `1`)

## Локальный запуск приложения

Для запуска достаточно запустить docker-compose. Структура БД автоматически развернется при старте Postgresql

```bash
cd deploy
docker-compose up -d
```

* Postgres - localhost:5432
* cats-api-photo - localhost:3001
* cats-api-like - localhost:3002
* cats-api-core - localhost:3003

## Разработка приложения

Запуск сервисов:

* база данных - из папки deploy запустить `docker-compose up -d pg`
* core - из папки cats-api-core запустить `npm run start:local`
* likes - из папки cats-api-likes запустить `npm run start:local`
* photos - из папки cats-api-photos запустить `npm run start:local`

## Процесс CI/CD

- Пушить в мастер нельзя
- Задача разрабатывается в ветке с кодом задачи (пр. `ACT-123`)
- По её готовности, создается Merge Request в `master`
- Автоматически собирается образ приложения. При необходимости, можно запустить джобу, которая выкатит данную ветку на QA контур
![CI/CD](/CI_CD.png)
- После проверки задачи, она мержится в `master`
- Для того что бы задеплоить приложение на PROD - необходимо создать `tag` с новой версией приложения, в формате `v1.2.3`
