const serverPort = process.env.NODE_PORT;
const pgUser = process.env.POSTGRES_USER;
const pgPass = process.env.POSTGRES_PASSWORD;
const pgDb = process.env.POSTGRES_DB;
const pgHost = process.env.POSTGRES_HOST;
const pgPort = process.env.POSTGRES_PORT;
const pathSaveFile = process.env.PATH_SAVE_FILE;
const serviceVersion = process.env.SERVICE_VERSION;
const swaggerBasePath = process.env.SWAGGER_BASE_PATH;

module.exports = {
  serverPort,
  pgUser,
  pgPass,
  pgDb,
  pgHost,
  pgPort,
  pathSaveFile,
  serviceVersion,
  swaggerBasePath,
};
