const supertest = require('supertest');
const {pool}=require('../src/storage');
const app = require('../src/routes');

function getRandomInt(min, max) {
    min = Math.ceil(min);
    max = Math.floor(max);
    let randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
    return randomInt;
}

function getRandomCatId(min, max, idsQuantity) {
    let catIds = [];
    for (let i = 1; i <= idsQuantity; i++) {
        let catIdsTemp = getRandomInt(min, max);
        while (catIds.includes(catIdsTemp)) {
            catIdsTemp = getRandomInt(min, max);
        }
        catIds.push(catIdsTemp);
        }
    return catIds;
}

function getRandomId(catIds) {
    let catIdMin = Math.min.apply(null, catIds);
    let catIdMax = Math.max.apply(null, catIds);
    let catId = getRandomInt(catIdMin, catIdMax);
    while (!catIds.includes(catId)) {
        catId = getRandomInt(catIdMin, catIdMax);
    }
    return catId;
}

function getMissedId(catIds) {
    let catIdMin = Math.min.apply(null, catIds);
    let catIdMax = Math.max.apply(null, catIds);
    let catId = getRandomInt(catIdMin, catIdMax);
    if (catIdMin == catIdMax) {
        catId = 2000000;
    } else {
        while (catIds.includes(catId)) {
            catId = getRandomInt(catIdMin, catIdMax);
        }
    }
    return catId;
}

function makeCatName(length) {
    let catName = '';
    const characters = 'АБВГДЕЖЗИКЛМНОПРСТУФХЦЧШЫЭЮЯабвгдежзиклмнопрстуфхцчшыэюя';
    for (let i = 0; i < length; i++ ) {
        catName += characters.charAt(Math.floor(Math.random() * characters.length));
    }
    return catName;
}

describe('Test get cat and save description', () => {
    /* Генерируем случайные неповторяющиеся id от ${min} до ${max} в количестве ${quantityOfIds} */

    const min = 1; // Должно быть больше 0 и меньше ${max}
    const max = 50; // Должно быть больше 0 и больше ${min}
    const quantityOfIds = 10; // Должно быть <= (${max} - ${min}) && > 0 , чтобы сработала функция getMissedId()
    const catIds = getRandomCatId(min, max, quantityOfIds);

    /* Чтобы работать в тесте с одной строкой нужно задать quantityOfIds = 1;
       чтобы работать с одним конкретным id нужно задать ${min} = ${max} и quantityOfIds = 1 */

    const catID = getRandomId(catIds); // Берем случайный idшник кота из сгенерированных
    const catMissedId = getMissedId(catIds); // Берем отсутствующий в сгенерированных idшник
    const catNullId = ''; // Пустой id
    const newCatDescription = 'Интересное описание'; // Описание, которое необходимо добавить коту

    /* Наполняем параметризованную таблицу из случайных неповторяющихся id от ${min} до ${max} в количестве
       ${quantityOfIds} случайными (до определенного количества записей) именами длиной ${lengthOfCatName}  */

    const lengthOfCatName = 8;

    beforeAll(async () => {
        await pool.query('TRUNCATE cats CASCADE');

        for (let i = 0; i < catIds.length; i++) {
            await Promise.all([pool.query(
                'INSERT INTO Cats(id, name, description, gender) VALUES ($1, $2, $3, $4)',
                [catIds[i], makeCatName(lengthOfCatName), 'Описание', 'male'])]);
        }
    });

    afterAll(() => {
        pool.end();
    });

    describe('#/cats/get-by-id', () => {

        it('should return cat', async () => {
            const catName =
                (await pool.query(`SELECT name FROM cats WHERE id = ${catID}`)).rows[0].name;
            const response = await supertest(app).get(`/cats/get-by-id?id=${catID}`);
            expect(response.status).toEqual(200);
            expect(response.body).toMatchObject(
                {
                    'cat': {
                        'id': catID,
                        'name': catName
                    }

                });
        });

        it('should return error 404', async () => {
            const response = await supertest(app).get(`/cats/get-by-id?id=${catMissedId}`);
            expect(response.status).toEqual(404);
            expect(response.body.output.statusCode).toEqual(404);
            expect(response.body.output.payload.statusCode).toEqual(404);
            expect(response.body.output.payload.error).toEqual('Not Found');
            expect(response.body.output.payload.message).toEqual(`Кот с id=${catMissedId} не найден`);
        });

        it('should return error 400', async () => {
            const response = await supertest(app).get(`/cats/get-by-id?id=${catNullId}`);
            expect(response.status).toEqual(400);
            expect(response.body.output.statusCode).toEqual(400);
            expect(response.body.output.payload.statusCode).toEqual(400);
            expect(response.body.output.payload.message).toEqual('Некорректный параметр ID');
        });
    });

    describe('#/cats/save-description', () => {

        it('should return cat with description', async () => {
            const response = await supertest(app)
                .post(`/cats/save-description`)
                .send({
                    'catId': catID,
                    'catDescription': newCatDescription
                });
            expect(response.status).toEqual(200);
            expect(response.body).toMatchObject(
                {
                    'id': catID,
                    'description': newCatDescription
                }
            );
        });

        it('should return error 404', async () => {
            const response = await supertest(app)
                .post(`/cats/save-description`)
                .send({
                    'catId': catMissedId,
                    'catDescription': newCatDescription
                });
            expect(response.status).toEqual(404);
            expect(response.body.output.statusCode).toEqual(404);
            expect(response.body.output.payload.statusCode).toEqual(404);
            expect(response.body.output.payload.message).toEqual(`Кот с id=${catMissedId} не найден`);
        });

        it('should return error 400', async () => {
            const response = await supertest(app)
                .post(`/cats/save-description`)
                .send({
                    'catId': catNullId,
                    'catDescription': newCatDescription
                });
            expect(response.status).toEqual(400);
            expect(response.body.output.statusCode).toEqual(400);
            expect(response.body.output.payload.statusCode).toEqual(400);
            expect(response.body.output.payload.message).toEqual('Некорректный параметр ID');
        });

        it('should return error 400', async () => {
            const response = await supertest(app)
                .post(`/cats/save-description`)
                .send({
                    'catId': catID,
                    'catDescription': ''
                });
            expect(response.status).toEqual(400);
            expect(response.body.output.statusCode).toEqual(400);
            expect(response.body.output.payload.statusCode).toEqual(400);
            expect(response.body.output.payload.message).toEqual('Некорректное описание');
        });
    });
});