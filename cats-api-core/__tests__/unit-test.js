const sinon = require('sinon');

class FakePool {
    constructor(args = {}) {
        this.rows = args.rows || [];
    }

    async query() {
        return Promise.resolve({rows: this.rows});
    }

    on() {
    }
}

describe('#saveCatDescription', () => {

    const sandbox = sinon.createSandbox();
    let pg;
    let poolStub;

    beforeEach(() => {
        pg = require('pg');
        poolStub = sandbox.stub(pg, 'Pool');
    });

    afterEach(() => {
        sandbox.restore();
        jest.resetModules();
    });

    it('should return row', async (done) => {
        const rows = [1, 'Арсений', 'Смешное описание', null, 'male', 0, 0]; // можно добавить свои значения
        poolStub.returns(new FakePool({rows}));
        const storage = require('../src/storage');
        expect(await storage.saveCatDescription()).toEqual(rows[0]);
        done();
    });

    it('should return object with 404', async (done) => {
        const rows = [];
        poolStub.returns(new FakePool({rows}));
        let thrownError;
        const storage = require('../src/storage');
        const errorObj = {
            code: 404,
            message: `Кот с id=undefined не найден`
        };

        try {
            await storage.saveCatDescription();
        } catch (error) {
            thrownError = error;
        }
        expect(thrownError).toEqual(errorObj);
        done();
    });
});