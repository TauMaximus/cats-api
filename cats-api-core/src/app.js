const {serverPort, pgDb} = require('./configs');
const {logger} = require('./logger');
const {pool} = require('./storage');
const app = require('./routes');

(async ()=>{
  try {
    logger.info('Try to connect database...');
    const databases = (await pool.query('SELECT datname FROM pg_database;')).rows;
    if (databases.filter((e) => e.datname === pgDb).length === 0) {
      throw new Error('Database not found');
    }
    logger.info('Database connect success!');
    app.listen(serverPort);
  } catch (err) {
    logger.error(err.message);
    process.exit(-1);
  }
})();
