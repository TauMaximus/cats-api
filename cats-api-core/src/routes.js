swaggerDocument = require('./swagger');
const express = require('express');
const swaggerUi = require('swagger-ui-express');
const catsStorage = require('./storage');
const {pool} = require('./storage');
const {logger} = require('./logger');
const bodyParser = require('body-parser');
const cors = require('cors');
const {isEmpty} = require('./helpers');
const {serviceVersion} = require('./configs');
const {addCats, allCats, findCatsByParams}=require('./controller');
const {sendResponse, sendError} = require('./helper-response');

const app = express();

app.use((err, req, res, next) => {
  logger.error(err.toString());
  next();
});

app.use(bodyParser.json());
app.use(cors());

app.get('/status', async (req, res) => {
  try {
    const bdVersion = (await pool.query('SELECT version()')).rows;
    return sendResponse(req, res, {
      version: serviceVersion,
      connectionToDB: {
        status: true,
        dbInfo: bdVersion,
      },
    });
  } catch (err) {
    return sendError(req, res, {code: 500, message: {
      version: serviceVersion,
      connectionToDB: {
        status: false,
        dbInfo: err.toString(),
      },
    }});
  }
});

app.post('/cats/add', async (req, res) => {
  const {cats} = req.body;
  try {
    sendResponse(req, res, await addCats(cats));
  } catch (err) {
    sendError(req, res, err);
  }
});

app.get('/cats/get-by-id', async (req, res) => {
  const {id} = req.query;
  if (isEmpty(id)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }

  try {
    sendResponse(req, res, {cat: await catsStorage.findCatById(id)});
  } catch (err) {
    sendError(req, res, err);
  }
});

app.post('/cats/search', async (req, res) => {
  const searchParams = {
    name: req.body.name,
    gender: req.body.gender,
    order: req.body.order,
  };

  try {
    sendResponse(req, res, await findCatsByParams(searchParams));
  } catch (err) {
    sendError(req, res, err);
  }
});

app.get('/cats/search-pattern', async (req, res) => {
  const {name, limit = 10} = req.query;

  if (isEmpty(name)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр имени'});
  }
  try {
    sendResponse(req, res, {cats: await catsStorage.findCatByNamePattern(name, limit)});
  } catch (err) {
    sendError(req, res, err);
  }
});

app.post('/cats/save-description', async (req, res) => {
  const {catId, catDescription} = req.body;

  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }
  if (isEmpty(catDescription)) {
    return sendError(req, res, {code: 400, message: 'Некорректное описание'});
  }

  try {
    sendResponse(req, res, await catsStorage.saveCatDescription(catId, catDescription));
  } catch (err) {
    sendError(req, res, err);
  }
});

app.get('/cats/validation', async (req, res) => {
  try {
    sendResponse(req, res, await catsStorage.findCatsValidationAllRules());
  } catch (err) {
    sendError(req, res, err);
  }
});

app.get('/cats/all', async (req, res) => {
  const {order, gender} = req.query;

  try {
    sendResponse(req, res, await allCats(gender, order));
  } catch (err) {
    sendError(req, res, err);
  }
});

app.delete('/cats/:catId/remove', async (req, res) =>{
  const {catId} = req.params;

  if (isEmpty(catId)) {
    return sendError(req, res, {code: 400, message: 'Некорректный параметр ID'});
  }

  try {
    sendResponse(req, res, await catsStorage.removeCats(catId));
  } catch (err) {
    sendError(req, res, err);
  }
});

app.use('/api-docs-ui', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.get('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

app.post('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

app.delete('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

app.put('*', (req, res)=>{
  return sendError(req, res, {code: 404, message: 'Страница не найдена'});
});

module.exports=app;
