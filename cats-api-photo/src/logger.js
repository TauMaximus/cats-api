const {createLogger: _createLogger, transports, format} = require('winston');
const {format: utilformat} = require('util');

const label = 'test';

function formatMessage(message) {
  if (typeof(message)==='string') {
    return message;
  }
  const responseJson = JSON.stringify(message.res.json);

  return `"${message.req.method} ${message.req.originalUrl}" ${message.res.status}`+
        ` "-" ${JSON.stringify(message.req.body)}`+
        ` "-" ${JSON.stringify(message.req.headers)}`+
        ` "-" ${responseJson.length < 300 ? responseJson : `${responseJson.slice(0, 200)}...`}`;
}

const logger = _createLogger({
  level: 'debug',
  transports: [new transports.Console()],
  format: format.combine(
      format.colorize(),
      format.timestamp(),
      format.splat(),
      format.label({label: label}),
      format.printf(({level, message, label, timestamp, durationMs, ...meta})=>{
        return utilformat(
            '%s [%s] %s %s %s %s',
            timestamp, label, level, formatMessage(message), Object.keys(meta).length ? JSON.stringify(meta) : '',
                durationMs ? ms(durationMs) : '',
        ).trim();
      }),
  ),
});


module.exports = {logger, formatMessage};
